<?php
/**
 * @var array $_
 * @var \OCP\IL10N $l
 * @var \OCP\Defaults $theme
 */
?>
<div id="firstrunwizard">
	<div class="firstrunwizard-header">

		<a id="closeWizard" class="close">
			<img class="svg" src="<?php p(image_path('core', 'actions/view-close.svg')); ?>">
		</a>
		<div class="logo">
			<p class="hidden-visually">
				<?php p($theme->getName()); ?>
			</p>
		</div>

		<h1><?php p($theme->getSlogan()); ?></h1>
		<p>Framagenda permet de consulter et de partager ses agendas, mais aussi ses carnets d'adresses et ses listes de tâches !</p>

	</div>

	<div class="firstrunwizard-content">

		<p>Pour synchroniser vos agendas sur plusieurs appareils,
			il faut télécharger un client de synchronisation correspondant à votre environnement compatible CalDAV. Les clients ci-dessous ont été testés avec succès.</p>
		<br>
		<div class="row">
			<h2>Bureau</h2>
			<div class="elem">
				<h3>Windows</h3>
				<ul>
					<li>Thunderbird <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/Thunderbird.html">(Tutoriel)</a></li>
					<li>Outlook avec le module complémentaire <a href="https://caldavsynchronizer.org/" target="_blank">CalDAV Synchronizer</a></li>
				</ul>
			</div>
			<div class="elem">
				<h3>Mac OS X</h3>
				<ul>
					<li>Thunderbird <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/Thunderbird.html">(Tutoriel)</a></li>
					<li>Calendrier (iCal) <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/ical.html">(Tutoriel)</a></li>
				</ul>
			</div>
			<div class="elem">
				<h3>GNU/Linux/*BSD</h3>
				<ul>
					<li>Thunderbird <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/Thunderbird.html">(Tutoriel)</a></li>
					<li>Gnome (Evolution, Contacts et Agenda) <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/gnome.html">(Tutoriel)</a></li>
					<li>KDE (KOrganizer)</li>
				</ul>
			</div>
			<h2>Mobile</h2>
			<div class="elem">
				<h3>Android</h3>
				<ul>
					<li>DAVdroid <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/Android.html">(Tutoriel)</a></li>
					<li>CalDAV-Sync</li>
				</ul>
			</div>
			<div class="elem">
				<h3>iOS</h3>
				<ul>
					<li>Paramètres de l'appareil <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/iOS.html">(Tutoriel)</a></li>
				</ul>
			</div>
			<div class="elem">
				<h3>Windows Phone</h3>
				<ul>
					<li>Paramètres de l'appareil <a target="_blank" href="https://docs.framasoft.org/fr/nextcloud/Synchronisation/WindowsPhone.html">(Tutoriel)</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<a target="_blank" class="button" href="https://docs.framasoft.org/fr/nextcloud/index.html">
				<img class="appsmall svg" src="<?php p(image_path('settings', 'help.svg')); ?>" /> <?php p($l->t('Documentation'));?>
			</a>
			<a class="button" href="https://framagenda.org/index.php/apps/calendar/">
				<img class="appsmall svg" src="<?php p(image_path('core', 'logo-icon.svg')); ?>" /> Se rendre sur Framagenda !
			</a>
		</div>

	</div>
</div>
